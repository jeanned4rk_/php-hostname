# php-hostname

This image simply prints the hostname of the container running the server.


Used to check your load balancing

https://hub.docker.com/r/jeanned4rk/php-hostname